require "minitest/autorun"
require_relative '../lib/log_parser'

class LogParserTest < Minitest::Test
    def test_should_parse_file_correctly
        parse_file = LogParser.list_of_page_views('test/files/log_parser_test_file.log')
        ordered_page_views_list = parse_file.first.first
        ordered_unique_page_views_list = parse_file.last.first

        assert_equal ordered_page_views_list.first,    'http://sanford.biz/cayla'
        assert_equal ordered_page_views_list.last,      10 

        assert_equal ordered_unique_page_views_list.first,    'http://aufderhartorphy.name/katrice'
        assert_equal ordered_unique_page_views_list.last,      2
    end

    def test_should_fail_if_irregular_path_to_file
        parse_file = LogParser.list_of_page_views('test/files/fail.log')
        assert_equal parse_file, nil
    end
end
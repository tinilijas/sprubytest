require 'uri'
require 'terminal-table'

module LogParser

    def self.list_of_page_views log
        puts '--- Script for page views has been started ---'

        return puts 'Argument provided is not a valid file' if log.nil? || !(File.file? log)
        # I was thinking of adding a validation for checking if the IP is correct, but as I saw none of them are correct :)
        file = File.open(log, 'r')
        file_data = file.read
        file.close

        return puts 'File is empty' if file_data.nil? # || file_data == ''
        ordered_page_views_list = get_data_for_ordered_page_views_list(file_data)

        ordered_page_views_list_table = Terminal::Table.new title: 'List of webpages with most page views',
                                                            headings: ['Page', 'Number of visits'], 
                                                            rows: ordered_page_views_list

        ordered_unique_page_views_list = get_data_for_unique_ordered_page_views_list(file_data.lines)

        ordered_unique_page_views_list_table = Terminal::Table.new title: 'List of webpages with most page views',
                                                             headings: ['Page', 'Uniq number of visits'], 
                                                             rows: ordered_unique_page_views_list

        puts ordered_page_views_list_table, ordered_unique_page_views_list_table

        [ordered_page_views_list, ordered_unique_page_views_list]
    end

    private

    def self.get_data_for_ordered_page_views_list data
        return if data.nil?

        urls = data&.scan(/\S*\/\S*/)
        urls.inject(Hash.new(0)) { |total, e| total[e] += 1 ;total }&.sort_by { |_k,v| v }&.reverse
    end

    def self.get_data_for_unique_ordered_page_views_list data
        return if data.nil?

        data = data.map(&:split).inject(Hash.new{ |h,k| h[k]=[] }){ |h,(k,v)| h[k] << v; h }
        data.each { |k, v| data[k] = v.uniq.count }.sort_by { |_k,v| v }&.reverse
    end
end

LogParser.list_of_page_views(ARGV[0])
